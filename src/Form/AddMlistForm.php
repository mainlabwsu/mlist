<?php
namespace Drupal\mlist\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implements an example form.
 */
class AddMlistForm extends FormBase {
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'add_mlist_form';
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        // Initialize the default values.
        $list_id = '';
        $default_sync  = array();
        $default_roles = array();
        $default_name  = '';
        $default_desc  = '';
        $default_sub   = '';
        $default_usub  = '';

        // If a list_id is provided to the form then this form should set the
        // defaults using the database fields.
        $action = t('Add');
        $btn_name = 'Add';

        // Get the list of available roles on the site.
        $allroles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
       
        $sync_roles = array();
        foreach ($allroles as $role => $obj) {
            $roles[$role] = $role;
            // We can't sync anonymous users so ignore that role.
            if ($role != 'anonymous user') {
                $sync_roles[$role] = $role;
            }
        }
        
        $form['list_id'] = array(
            '#type'  => 'value',
            '#value' => $list_id,
        );
        $form['list_name'] = array(
            '#type'          => 'textfield',
            '#title'         => t('Mailing List Name'),
            '#description'   => t('Please enter the name of the MailMan mailing list.  This should be the name that appears in the MailMan configuration settings'),
            '#required'      => TRUE,
            '#default_value' => $default_name,
        );
        $form['description'] = array(
            '#type'          => 'textarea',
            '#title'         => t('Description'),
            '#description'   => t('Please enter a description for this database that site visitors will read.'),
            '#default_value' => $default_desc,
            '#required'      => TRUE,
        );
        $form['subscribe_email'] = array(
            '#type'          => 'textfield',
            '#title'         => t('Subscription email'),
            '#description'   => t('Please enter the mailman subscription email address. The typical mailman subscribe email address for a list is: [list name]-subscribe@[domain].'),
            '#default_value' => $default_sub,
            '#required'      => TRUE,
            '#element_validate' => array('mlist_validate_email'),
        );
        $form['unsubscribe_email'] = array(
            '#type'          => 'textfield',
            '#title'         => t('Unsubscription email'),
            '#description'   => t('Please enter the mailman unsubscription email address. The typical mailman subscribe email address for a list is: [list name]-unsubscribe@[domain].'),
            '#default_value' => $default_usub,
            '#required'      => TRUE,
            '#element_validate' => array('mlist_validate_email'),
        );
        $form['roles'] = array(
            '#type'          => 'checkboxes',
            '#title'         => t('Allowed Roles'),
            '#description'   => t('Please select the roles that should be able to subscribe/unsubscribe to the list.'),
            '#options'       => $roles,
            '#required'      => TRUE,
            '#default_value' => $default_roles,
        );
        $form['sync'] = array(
            '#type'          => 'checkboxes',
            '#title'         => t('Sync mailing list members with the following roles'),
            '#description'   => t('Sync Mailman mailing list members for the selected roles. This option only works when the "drush mailman-sync" command can be executed on the same server where the Mailman mail server is installed.'),
            '#options'       => $sync_roles,
            '#default_value' => $default_sync,
        );
        $form['add'] = array(
            '#type'  => 'submit',
            '#value' => $action,
            '#name'  => $btn_name,
        );
        $form['#tree'] = TRUE;
        $form['#prefix'] = '<div id="mlist_add_form_div">';
        $form['#suffix'] = '</div>';
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        $db = \Drupal::database();
        $list_name = trim($form_state->getValue('list_name'));
        
        $new_list_name = $db->select('mailing_lists', 'ml')
        ->fields('ml', array('list_name'))
        ->condition('list_name', $list_name)
        ->execute()
        ->fetchField();
        if ($new_list_name) {
            $form_state->setErrorByName('list_name', $this->t('The list name already exists.'));
        }
        
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $db = \Drupal::database();
        $list_id           = $form_state->getValue('list_id');
        $list_name         = trim($form_state->getValue('list_name'));
        $description       = trim($form_state->getValue('description'));
        $subscribe_email   = trim($form_state->getValue('subscribe_email'));
        $unsubscribe_email = trim($form_state->getValue('unsubscribe_email'));
        $roles             = $form_state->getValue('roles');
        $sync              = $form_state->getValue('sync');
        $sync_rids         = '';
        $form_state->setRedirectUrl(new Url('mailing_lists.show_lists'));

        // Iterate through the roles that the users requested to be synced and
        // construct a comma-delimited list of role IDs.
        foreach ($sync as $s_rid => $s_selected) {
            if ($s_selected) {
                $sync_rids .= $s_rid;
                $sync_rids .= ',';
            }
        }
        // Remove the trailing comma.
        $sync_rids = substr($sync_rids, 0, -1);
        
        // Now handle the response based on the button clicked.
        $transaction = $db->startTransaction();
        
        try {
            // Add the list.
            $list_id = $db->insert('mailing_lists')
            ->fields(array(
                'list_name'         => $list_name,
                'description'       => $description,
                'subscribe_email'   => $subscribe_email,
                'unsubscribe_email' => $unsubscribe_email,
                'sync'              => $sync_rids,
            ))
            ->execute();

            // Add the allowed roles for this list.  We want to first remove any
            // roles that exists and then add in the newly selected ones
            // first remove all existing roles for this list.
            $db->delete('mailing_lists_roles')
            ->condition('list_id', $list_id)
            ->execute();
            // Now add the roles.
            foreach ($roles as $rid => $selected) {
                if ($selected) {
                    $db->insert('mailing_lists_roles')
                    ->fields(array('list_id' => $list_id, 'rid' => $rid))
                    ->execute();
                }
            }
        }
        catch (Exception $e) {
            \Drupal::logger('mlist')->error($e->getMessage());
            $this->messenger()->addError($this->t('Failed to add mailing list.'));
            $transaction->rollback();
            return;
        }
        $this->messenger()->addMessage($this->t('Mailing list saved.'));
    }
    
}