<?php
namespace Drupal\mlist\Form;

require 'lib/Exception.php';
require 'lib/PHPMailer.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements an example form.
 */
class MlistSubscribeForm extends FormBase {
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'mlist_subscribe_form';
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $user = \Drupal::currentUser();
        
        $lists = $this->getLists();
        if (count($lists) > 0) {
            
            // Provide some instructions.
            $form['intro'] = array(
                '#markup' => t('Subscribe or unsubscribe to a mailing list by entering your email address and selecting the checkbox for the mailing list. You will receive an email with further instructions to complete the process.'),
            );
            
            // If the user is anonymous then provide a text field for the email.
            if ($user->id() == 0) {
                $form['email'] = array(
                    '#type'      => 'textfield',
                    '#title'     => $this->t('Your Email address'),
                    '#size'      => 20,
                    '#required'  => TRUE,
                    '#maxlength' => 255,
                    '#element_validate' => array('mlist_validate_email'),
                );
            }
            // Else use the registered email address for the user.
            else {
                $mail = $user->getEmail();
                $form['email'] = array(
                    '#type'      => 'value',
                    '#value'     => $mail,
                );
                $form['your_email'] = array(
                    '#markup'    => '<p><label>' . $this->t('Your email address: %email', array('%email' => $mail)) . '</label></p>',
                );
            }
            
            $form['fname'] = array(
                '#type'      => 'textfield',
                '#title'     => $this->t('First Name'),
                '#size'      => 20,
                '#required'  => TRUE,
                '#maxlength' => 255,
            );
            
            $form['lname'] = array(
                '#type'      => 'textfield',
                '#title'     => $this->t('Last Name'),
                '#size'      => 20,
                '#required'  => TRUE,
                '#maxlength' => 255,
            );
            
            $form['orgn'] = array(
                '#type'      => 'textfield',
                '#title'     => $this->t('Organization'),
                '#size'      => 20,
                '#required'  => TRUE,
                '#maxlength' => 255,
            );
            
            // Iterate through the lists and create a checkbox field for each one.
            $options = array();
            foreach ($lists as $list_id => $info) {
                $options[$list_id] = $this->t($info['name'] . '. (' . $info['description'] . ') ');
            }
            $form['lists'] = array(
                '#type'  => 'checkboxes',
                '#title' => $this->t('Please select one or more lists.'),
                '#required'  => TRUE,
                '#options' => $options,
            );
            
            // Add in the subscribe and unsubscribe buttons.
            $form['subscribe'] = array(
                '#type' => 'submit',
                '#value' => $this->t('subscribe'),
                '#name' => 'subscribe',
                '#submit' => array(array($this, 'submitSubscribe')),
            );
            $form['unsubscribe'] = array(
                '#type' => 'submit',
                '#value' => $this->t('unsubscribe'),
                '#name' => 'unsubscribe',
                '#submit' => array(array($this, 'submitUnsubscribe')),
            );
        }
        else {
            $form['intro'] = array(
                '#markup' => $this->t('Sorry, there are no lists currently available for subscription.'),
            );
        }
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
    }
    
    public function submitForm(array &$form, FormStateInterface $form_state) {        
    }
    
    public function submitSubscribe(array &$form, FormStateInterface $form_state) {
        $this->submitAction($form, $form_state, 'subscribe');
    }
    
    public function submitUnsubscribe(array &$form, FormStateInterface $form_state) {
        $this->submitAction($form, $form_state, 'unsubscribe');
    }
    /**
     * {@inheritdoc}
     */
    public function submitAction(array &$form, FormStateInterface $form_state, $action = 'subscribe') {
        
        $from = trim($form_state->getValue('email'));
        $fname = trim($form_state->getValue('fname'));
        $lname = trim($form_state->getValue('lname'));
        $orgn = trim($form_state->getValue('orgn'));
        $display = $orgn ? "$fname $lname" . ', ' . $orgn : "$fname $lname";
        $params = array();
        $lists = $this->getLists();
        $selected_lists = array_filter($form_state->getValue('lists'));
        
        // If the user supplied an email address then send the email
        // the Mailman list server.
        if ($from) {
            
            // Iterate through all of the available lists and for those that
            // the user selected send an email.
            foreach ($lists as $list_id => $info) {
                if (in_array($list_id, $selected_lists)) {
                    $list_email = $info[$action];
                    $params['subject'] = $action;
                    $params['from'] = $from;
                    //$mailManager = \Drupal::service('plugin.manager.mail');
                    //$message = $mailManager->mail('mlist', $action, $list_email, \Drupal::currentUser()->getPreferredLangcode(), $params, $from, TRUE);
                    
                    
                    $headers = array('From' => "$display <$from>");
                    mail($list_email, $action, $action, $headers);
                    
                    
                    $mail = new PHPMailer(true);                    
                    $mail->setFrom($from, $display);
                    $mail->addAddress($list_email);     // Add a recipient
                    $mail->addReplyTo($from, $display);
                    $mail->Subject = $action;
                    $mail->Body = $action;
                    //$mail->send();
                    
                    // Inform the user that an email has been sent.
              //      if ($message['result']) {
                        $this->messenger()->addMessage(
                            t('An email has been sent to, %from, with details to complete the %action process to the %list_name mailing list. Please follow the instructions in the email to continue.',
                                array(
                                    '%from' => $from,
                                    '%action' => $action,
                                    '%list_name' => $lists[$list_id]['name'],
                                )
                                )
                            );
            //        }
                }
            }
        }
    }
    
    function getLists() {
        $user = \Drupal::currentUser();
        
        $sql = '
            SELECT
              ML.list_name, ML.description, ML.subscribe_email,
              ML.unsubscribe_email, ML.list_id, MLR.rid as role_name
            FROM {mailing_lists} ML
               LEFT JOIN {mailing_lists_roles} MLR ON ML.list_id = MLR.list_id
            ORDER BY list_name
          ';
        $db = \Drupal::database();
        $lists = $db->query($sql);
        $mylists = array();
        foreach ($lists AS $list) {
            
            // Check to see if user has the access to this list, if so,
            // include the list in the result.
            if (in_array($list->role_name, array_values($user->getRoles())) or
                \Drupal::currentUser()->hasPermission('manage mailing lists')) {
                    // Don't add the list more than once.
                    if (!in_array($list->list_id, $mylists)) {
                        $mylists[$list->list_id] = array(
                            'name'        => $list->list_name,
                            'description' => $list->description,
                            'subscribe'   => $list->subscribe_email,
                            'unsubscribe' => $list->unsubscribe_email,
                        );
                    }
                }
        }
        return $mylists;
    }
    
}
