<?php
namespace Drupal\mlist\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements an example form.
 */
class EditMlistForm extends FormBase {
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'edit_mlist_form';
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $mlist_id = NULL) {
        $db = \Drupal::database();
        // If a list_id is provided to the form then this form should set the
        // defaults using the database fields.

        $action = t('Update');
        $btn_name = 'Update';
        $list = $db->select('mailing_lists', 'ml')
        ->fields('ml')
        ->condition('list_id', $mlist_id)
        ->execute()
        ->fetchObject();
        if (!$default_name) {
            $default_name = $list->list_name;
        }
        if (!$default_desc) {
            $default_desc = $list->description;
        }
        if (!$default_sub) {
            $default_sub = $list->subscribe_email;
        }
        if (!$default_usub) {
            $default_usub = $list->unsubscribe_email;
        }
        $default_sync = explode(',', $list->sync);
        $assigned_roles = $db->select('mailing_lists_roles', 'mlr') 
        ->fields('mlr', array('rid'))
        ->condition('list_id', $mlist_id)
        ->execute();
        foreach ($assigned_roles as $assigned_role) {
            $default_roles[] = $assigned_role->rid;
        }
    
        // Get the list of available roles on the site.
        $allroles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
        
        $sync_roles = array();
        foreach ($allroles as $role => $obj) {
            $roles[$role] = $role;
            // We can't sync anonymous users so ignore that role.
            if ($role != 'anonymous user') {
                $sync_roles[$role] = $role;
            }
        }
        
        $form['list_id'] = array(
            '#type'  => 'value',
            '#value' => $mlist_id,
        );
        $form['list_name'] = array(
            '#type'          => 'textfield',
            '#title'         => t('Mailing List Name'),
            '#description'   => t('Please enter the name of the MailMan mailing list.  This should be the name that appears in the MailMan configuration settings'),
            '#required'      => TRUE,
            '#default_value' => $default_name,
        );
        $form['description'] = array(
            '#type'          => 'textarea',
            '#title'         => t('Description'),
            '#description'   => t('Please enter a description for this database that site visitors will read.'),
            '#default_value' => $default_desc,
            '#required'      => TRUE,
        );
        $form['subscribe_email'] = array(
            '#type'          => 'textfield',
            '#title'         => t('Subscription email'),
            '#description'   => t('Please enter the mailman subscription email address. The typical mailman subscribe email address for a list is: [list name]-subscribe@[domain].'),
            '#default_value' => $default_sub,
            '#required'      => TRUE,
        );
        $form['unsubscribe_email'] = array(
            '#type'          => 'textfield',
            '#title'         => t('Unsubscription email'),
            '#description'   => t('Please enter the mailman unsubscription email address. The typical mailman subscribe email address for a list is: [list name]-unsubscribe@[domain].'),
            '#default_value' => $default_usub,
            '#required'      => TRUE,
        );
        $form['roles'] = array(
            '#type'          => 'checkboxes',
            '#title'         => t('Allowed Roles'),
            '#description'   => t('Please select the roles that should be able to subscribe/unsubscribe to the list.'),
            '#options'       => $roles,
            '#required'      => TRUE,
            '#default_value' => $default_roles,
        );
        $form['sync'] = array(
            '#type'          => 'checkboxes',
            '#title'         => t('Sync mailing list members with the following roles'),
            '#description'   => t('Sync Mailman mailing list members for the selected roles. This option only works when the "drush mailman-sync" command can be executed on the same server where the Mailman mail server is installed.'),
            '#options'       => $sync_roles,
            '#default_value' => $default_sync,
        );
        $form['add'] = array(
            '#type'  => 'submit',
            '#value' => $action,
            '#name'  => $btn_name,
        );
        $form['actions']['cancel'] = array(
            '#type' => 'submit',
            '#value' => t('Cancel'),
            '#submit' => array(array($this, 'cancelAction')),
        );
        $form['#tree'] = TRUE;
        $form['#prefix'] = '<div id="mlist_edit_form_div">';
        $form['#suffix'] = '</div>';
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        $db = \Drupal::database();
        $list_name = trim($form_state->getValue('list_name'));
        $list_id   = $form_state->getValue('list_id');
           
        // Check if the list name has changed and if so, make sure it is unique.
        $orig_list_name = $db->select('mailing_lists', 'ml')
        ->fields('ml', array('list_name'))
        ->condition('list_id', $list_id)
        ->execute()
        ->fetchField();
        // Make sure the new name is not a duplicate.
        if ($orig_list_name != $list_name) {
            $new_list_name = $db->select('mailing_lists', 'ml')
            ->fields('ml', array('list_name'))
            ->condition('list_name', $list_name)
            ->condition('list_id', $list_id, '<>')
            ->execute()
            ->fetchField();
            if ($new_list_name) {
                $form_state->setErrorByName('list_name', $this->t('The list name already exists.'));
            }
        }    
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $db = \Drupal::database();
        $list_id           = $form_state->getValue('list_id');
        $list_name         = trim($form_state->getValue('list_name'));
        $description       = trim($form_state->getValue('description'));
        $subscribe_email   = trim($form_state->getValue('subscribe_email'));
        $unsubscribe_email = trim($form_state->getValue('unsubscribe_email'));
        $roles             = $form_state->getValue('roles');
        $sync              = $form_state->getValue('sync');
        $sync_rids         = '';
        $form_state->setRedirectUrl(new Url('mailing_lists.show_lists'));
            
        // Iterate through the roles that the users requested to be synced and
        // construct a comma-delimited list of role IDs.
        foreach ($sync as $s_rid => $s_selected) {
            if ($s_selected) {
                $sync_rids .= $s_rid;
                $sync_rids .= ',';
            }
        }
        // Remove the trailing comma.
        $sync_rids = substr($sync_rids, 0, -1);
        
        // Now handle the response based on the button clicked.
        $transaction = $db->startTransaction();
        
        try {
            // If a list id is provided then we will handle an update or a delete.
             $db->update('mailing_lists')
             ->fields(array(
             'list_name'         => $list_name,
             'description'       => $description,
             'subscribe_email'   => $subscribe_email,
             'unsubscribe_email' => $unsubscribe_email,
             'sync'              => $sync_rids,
             ))
             ->condition('list_id', $list_id)
             ->execute();             
            
            // Add the allowed roles for this list.  We want to first remove any
            // roles that exists and then add in the newly selected ones
            // first remove all existing roles for this list.
            $db->delete('mailing_lists_roles')
            ->condition('list_id', $list_id)
            ->execute();
            // Now add the roles.
            foreach ($roles as $rid => $selected) {
                if ($selected) {
                    $db->insert('mailing_lists_roles')
                    ->fields(array('list_id' => $list_id, 'rid' => $rid))
                    ->execute();
                }
            }
        }
        catch (Exception $e) {
            \Drupal::logger('mlist')->error($e->getMessage());
            $this->messenger()->addError($this->t('Failed to update mailing list.'));
            $transaction->rollback();
            return;
        }
        $this->messenger()->addMessage($this->t('Mailing list updated.'));
    }
    
    
    public function cancelAction (array &$form, FormStateInterface $form_state) {
        $this->messenger()->addMessage($this->t(('Operation cancelled')));
        $response = new RedirectResponse(\Drupal\Core\Url::fromRoute('mailing_lists.show_lists')->toString());
        $response->send();
    }
    
}