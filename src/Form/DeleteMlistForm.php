<?php
namespace Drupal\mlist\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements an example form.
 */
class DeleteMlistForm extends FormBase {
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'delete_mlist_form';
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $mlist_id = NULL) {
        $db = \Drupal::database();       
        $list_name = $db->select('mailing_lists', 'ml')
        ->fields('ml', array('list_name'))
        ->condition('list_id', $mlist_id)
        ->execute()
        ->fetchField();
        $form['list_id'] = array(
            '#type'  => 'value',
            '#value' => $mlist_id,
        );
        $form['extra'] = array(
            '#type' => 'item',
            '#markup' => t('This will remove the list from this site. Users will no longer be able to subscribe or unsubscribe using this site.'),
        );
        $form['description'] = array(
            '#type' => 'item',
            '#markup' => t('Are you sure you want to delete the list: %list_name?', array('%list_name' => $list_name)),
        );
        $form['actions'] = array('#type' => 'actions');
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Confirm'),
        );
        $form['actions']['cancel'] = array(
            '#type' => 'submit',
            '#value' => t('Cancel'),
            '#submit' => array(array($this, 'cancelAction')),
        );
        // By default, render the form using theme_confirm_form().
        if (!isset($form['#theme'])) {
            $form['#theme'] = 'confirm_form';
        }
        return $form;
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
    }
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $db = \Drupal::database();
        $list_id = $form_state->getValue('list_id');
        $form_state->setRedirectUrl(new Url('mailing_lists.show_lists'));
        
        // Delete the list and ancillary information.
        $transaction = $db->startTransaction();
        try {
            $db->delete('mailing_lists')
            ->condition('list_id', $list_id)
            ->execute();
            $db->delete('mailing_lists_roles')
            ->condition('list_id', $list_id)
            ->execute();
        }
        catch (Exception $e) {
            \Drupal::logger('mlist')->error($e->getMessage());
            $this->messenger()->addError($this->t('Failed to delete mailing list.'), 'error');
            $transaction->rollback();
            return;
        }
        $this->messenger()->addMessage($this->t(('Mailing list deleted')));
    }
    
    public function cancelAction (array &$form, FormStateInterface $form_state) {
        $this->messenger()->addMessage($this->t(('Operation cancelled')));
        $response = new RedirectResponse(\Drupal\Core\Url::fromRoute('mailing_lists.show_lists')->toString());
        $response->send();
    }
    
}