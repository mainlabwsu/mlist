<?php 
namespace Drupal\mlist\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

class MlistsController extends ControllerBase {
    
    public function show() {
        $db = \Drupal::database();
        $header = array('List Name', 'Description', array('data' => 'Actions', 'colspan' => 2));
        $rows = array();
        
        // Get all of the lists and create a table row for each one.
        $lists = $db->select('mailing_lists', 'ml')
        ->fields('ml')
        ->execute();
        
        foreach ($lists as $list) {
            $rows[] = array(
                $this->t($list->list_name),
                $this->t($list->description),
                Link::fromTextAndUrl($this->t('Edit'), Url::fromRoute('mailing_lists.edit_list_form', array('mlist_id' => $list->list_id)))->toString(),
                Link::fromTextAndUrl($this->t('Delete'), Url::fromRoute('mailing_lists.delete_list_form', array('mlist_id' => $list->list_id)))->toString()
            );
        }
        $table = array(
            '#header' => $header,
            '#rows' => $rows,
            '#attributes' => array(),
            '#sticky' => TRUE,
            '#caption' => '',
            '#colgroups' => array(),
            '#empty' => $this->t('There are currently no mailing lists'),
            '#theme' => 'table',
        );
        return $table;
    }
}