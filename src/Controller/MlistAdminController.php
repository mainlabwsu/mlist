<?php 
namespace Drupal\mlist\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

class MlistAdminController extends ControllerBase {
    
    public function show() {
        return array(
            '#markup' => 
            '<ul class="admin-list">
                 <li>
                    <a href="/admin/config/system/mlist/new">Add a Mailing List</a>
                    <div class="description">Add a new Mailman mailing list</div>
                 </li>
                 <li>
                    <a href="/admin/config/system/mlist/show">Show Mailing Lists</a>
                    <div class="description">Edit a Mailman mailing list that has already been added.</div>
                 </li>
             </ul>'
        );
    }
}