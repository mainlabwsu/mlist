<?php 
namespace Drupal\mlist\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "mlist_block",
 *   admin_label = @Translation("Mailing List Subscribe"),
 *   category = @Translation("Mailing List"),
 * )
 */
class MlistBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\mlist\Form\MlistSubscribeForm');
    return $form;
  }

}
